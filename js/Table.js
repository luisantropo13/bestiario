class Table{
    static createEntry(id,imgurl,nombre,raza,pais,debilidad,descripcion){
        let html = "";
        html+="<tr id='Monstruo_"+id+"'>";
        html+='<td><img src="media/'+imgurl+'"></td>';
        html+='<td>'+ nombre +'</td>';
        html+='<td>'+ raza +'</td>';
        html+='<td>'+ pais +'</td>';
        html+='<td>'+ debilidad +'</td>';
        html+='<td>'+ descripcion +'</td>';
        html+='<td>'+ `<button id="butadd" onclick="Ventana.open( [${id}]    , 'add',800,530);">Modificar </button> 
                       <button id="butadd" onclick="Table.removeByID(${id})">Borrar </button>`+'</td>'
        html+='</tr>\n';
        return html;
    };

     static async refresh(){
        Table.create(await MonstroLoader.readAllMonstro());
    }

    static removeByID(id){
       MonstroLoader.deleteMonstroByID(id);
       let e =  document.querySelector(`#Monstruo_${id}`);
       e.parentElement.removeChild(e);
    }

    static destroyTable(){
        document.getElementById('beast_list').innerHTML= "";
    }

    static create(perfil){

        let tabla =  document.getElementById('beast_list');
        let html = "<table  border=1>";
        html+= `<thead><tr><th> Foto </th>
                           <th> Nombre </th>
                           <th> Raza </th>
                           <th> Pais </th>
                           <th> Debilidad </th>
                           <th> Descripción </th>
                           <th> Editar/Eliminar </th></tr></thead>`
        html += "<tbody id='beast_table'>";

        for (const x of perfil) {
            html+=this.createEntry(x.getID(),
                                   x.getImageURL(), 
                                   x.getNombre(),
                                   x.getRaza(),
                                   x.getPais(),
                                   x.getDebilidad(),
                                   x.getDescripcion());
        }
        html+="</tbody></table>";
      
        tabla.innerHTML = html;
    };

    static addEntry(url,nombre,escencial,description){
        document.getElementById("beast_table").innerHTML+=this.createEntry(url,nombre,escencial,description);
    }
}




