
class MonstroLoader{


    static async convertArrayToMostro( LOSMOSTROS ) 
    {
        const MOSTROARRAY = [];
        let actuMostro;
        for( let i = 0; i < LOSMOSTROS.length; i++ )
        {
            actuMostro = LOSMOSTROS[i];

            MOSTROARRAY.push( await Monstro.createFromJSON( actuMostro ) );
        }

        return MOSTROARRAY;
    }

    //Lee el json y lo convierte en una array 
    static async readAllMonstro(){
       let fs = require( "fs" );
       const PATH = "./data/perfil.json";

        const LOSMOSTROS  = JSON.parse( await fs.readFileSync(PATH,"utf-8"));
        

        return await MonstroLoader.convertArrayToMostro( LOSMOSTROS );
    }


    //Esta parte es para incrementar el id en el json
    static  async getIDIncrement( ) 
    {
        return ( await MonstroLoader.getLastID( ) + 1 );
    }

    //Obtiene el ultimo ID 
    static async getLastID( ) 
    {
        let mostroArray = await MonstroLoader.readAllMonstro( );
        let id = 0;

        for( let i = 0; i < mostroArray.length; i++ ) 
        {
            if( mostroArray[i].getID() > id ) 
                id = mostroArray[i].getID();
        }
        return id;
    }

    static async findMonstroByID( id ) 
    {
        const mostroARRAY = await MonstroLoader.readAllMonstro( );
        let actuMostro;

        for( let i = 0; i < mostroARRAY.length; i++ ) 
        {
            actuMostro = mostroARRAY[i];

            if( actuMostro.getID( ) == id ) 
                return actuMostro;
        }

        return null;
    }

    static async deleteMonstroByID( idMostro) 
    {
        const fs = require("fs");
        const PATH = "./data/perfil.json";

        let mostro = await this.findMonstroByID( idMostro );
        console.log(mostro.getFKStats());
        let data = await MonstroLoader.readAllMonstro( );   
        
        await fs.writeFileSync(PATH, JSON.stringify(data.filter( e => e.getID() != idMostro ) ) );   
        await fs.unlinkSync( "media/"+mostro.getImageURL( ) );



        MonstroStatLoader.deleteStatsByID( mostro.getFKStats( ) );
    }

    //Añade al monstro a json
    static async saveMonstro( theMostro ) {
        const fs = require("fs");
        const PATH = "./data/perfil.json";


        let data   = await MonstroLoader.readAllMonstro( );
        let mostro = await MonstroLoader.findMonstroByID( theMostro.getID( ) );

//      Crear mostro.
        if( mostro  == null ) 
        {
            data.push( theMostro );

        }else{ // Guardar mostro.
            
            for( let i = 0; i < data.length; i++ )
            {
                if( data[i].getID() == theMostro.getID() ) 
                {
                    data[i] = theMostro;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 break;
                }
            }

            MonstroStatLoader.modifyStatsByID( theMostro.getFKStats() , theMostro.getRaza( ) , theMostro.getPais( ) );
                
        }
        
    
    
        fs.writeFileSync(PATH, JSON.stringify(data));    
    }
}




