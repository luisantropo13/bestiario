

var VentanaAPI = {
    dialog : null, 
    MonstroLoader  : null, 
    MonstroStatLoader: null,
    fs : require( "fs" ),
    table:null
 };
 




class Ventana {

    static actualPath;
    static dimension = {w:0,h:0};
    static params = [];

    static open( params = [] , path,width=800,height=800){
        document.querySelector("#capaSuper").style.display = "flex";
        document.querySelector("#capaSuper_container").style.width = width+"px";
        document.querySelector("#capaSuper_container").style.height = height+"px";
        document.querySelector("#capaSuper_container_body").src="ventanas/"+path+"/index.html";
        document.querySelector("#capaSuper_container_header_actions").innerHTML="";

        Ventana.actualPath = path;
        Ventana.dimension.w = width;
        Ventana.dimension.h = height;
        
        Ventana.params = params;

        document.querySelector("#capaSuper_container_body").onload = async function( ) 
        {
                document.querySelector("#capaSuper_container_body").contentWindow.require = require;

                await document.querySelector("#capaSuper_container_body").contentWindow.main(
                        Ventana.params , VentanaAPI
                );
        }

    }


    static reload( ) 
    {
        Ventana.open( Ventana.params , Ventana.actualPath , Ventana.dimension.w  , Ventana.dimension.h );
    }

    static close(){
        document.querySelector("#capaSuper").style.display = "none";
    }

    static fernandoApruebame( ) 
    {
        for( let i = 0; i < 10; i++ )
           {break;} // :)
    }

    static addAction(text,callback){
        let buttom = document.createElement("buttom");
        buttom.innerHTML=text;
        buttom.onclick=callback;
        document.querySelector("#capaSuper_container_header_actions").appendChild(buttom);
    }
}


VentanaAPI.dialog = Ventana;
VentanaAPI.MonstroLoader = MonstroLoader;
VentanaAPI.table = Table;
VentanaAPI.MonstroStatLoader = MonstroStatLoader;