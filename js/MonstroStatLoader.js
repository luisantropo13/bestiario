class MonstroStatLoader{


    static async loadFileDocumentXMLStats( ) 
    {
        const  fs = require("fs");
        var Stat = await fs.readFileSync("./data/estadistica.xml");
        var Doc = new DOMParser().parseFromString(Stat,"text/xml");

        return Doc;
    }

    static async saveFileDocumentStats( doc  ) 
    {
        await require( "fs" ).writeFileSync( "./data/estadistica.xml" , 
                new XMLSerializer().serializeToString(doc.documentElement) 
        );
    }

    static async getIncrementID( ) 
    {
        const xmlDoc = await this.loadFileDocumentXMLStats( );
        const nodes = xmlDoc.evaluate("/estadistica/data[last()]", xmlDoc, null, XPathResult.ANY_TYPE,null);
        let result = nodes.iterateNext();

        return parseInt( result.getAttribute( "id" ) )+1;
    }





    static async createStatMostroAndWrite( raza , pais  ) 
    {
            const DOC = await this.loadFileDocumentXMLStats( );
            const ID  = await this.getIncrementID( );

            const TAG = DOC.createElement( "data" );
                  TAG.setAttribute( "id" , ID );
                  const TAG_RAZA = DOC.createElement( "raza" );
                        TAG_RAZA.innerHTML = raza;
                  const TAG_PAIS = DOC.createElement( "pais" );
                        TAG_PAIS.innerHTML = pais;


                        TAG.appendChild( TAG_RAZA );
                        TAG.appendChild( TAG_PAIS );

                DOC.childNodes[0].appendChild( TAG );     
                this.saveFileDocumentStats( DOC );
      

                return ID;
    }

    static async deleteStatsByID( id ) 
    {
        const MOSCO = await this.loadFileDocumentXMLStats( );
        const nodes = MOSCO.evaluate( `/estadistica/data[@id='${id}']` , MOSCO, null, XPathResult.ANY_TYPE,null);
        let result  = nodes.iterateNext();

        result.parentNode.removeChild( result );

       

        
        this.saveFileDocumentStats( MOSCO );
    }

    static async modifyStatsByID( id , raza , pais ) 
    {
        const MOSCO = await this.loadFileDocumentXMLStats( );
        const nodes = MOSCO.evaluate( `/estadistica/data[@id='${id}']` , MOSCO, null, XPathResult.ANY_TYPE,null);
        let result  = nodes.iterateNext();

        result.getElementsByTagName("pais")[0].innerHTML = pais;
        result.getElementsByTagName("raza")[0].innerHTML = raza;
        
        this.saveFileDocumentStats( MOSCO );
    }

    static async loadMonstroStatsByID( id ){
       
        const Doc   = await this.loadFileDocumentXMLStats( );
        const nodes = Doc.evaluate(`/estadistica/data[@id='${id}']`, Doc, null, XPathResult.ANY_TYPE,null);
        let result  = nodes.iterateNext();
        
        return ( !result ) ? null : {
            pais: result.getElementsByTagName("pais")[0].innerHTML , 
            raza: result.getElementsByTagName("raza")[0].innerHTML , 
        };
        
    }
}