
    class Ventana
    {
        static async init( ) 
        {
            switch( Ventana.getAction() )
            {
                case "modify":
                    api.dialog.addAction("Guardar", async function(){
                        const MOSTRO = await api.MonstroLoader.findMonstroByID( api.dialog.params[0] );
                        MOSTRO.modifyByJSON(Formulario.getForm());
                        await MOSTRO.saved();
                        alert("Has Guardado");
                        api.dialog.close();
                        api.table.refresh();
                    });
                break;

                case "add":
                    api.dialog.addAction("Crear", async function(){
                            const NUEVOMOSTRO = await Formulario.createMonstro( );
                                await NUEVOMOSTRO.saved();
                                alert( "Mostro creado" );
                                api.dialog.close();
                                api.table.refresh();
                    });
                break;
            }

                
            api.dialog.addAction("Reset",function(){
                api.dialog.reload();
        });
        }
        
        static getAction( ) 
        {
            if( api.dialog.params[0] == null)
            return "add";

            return "modify";
        }


    }

    async function main( arguments , api) 
    {
        window.api = api;
        window.MonstroStatLoader = api.MonstroStatLoader;

        Ventana.init();
    

        
        if( Ventana.getAction( ) == "modify" ) 
        {
            const MOSTRO = await api.MonstroLoader.findMonstroByID( api.dialog.params[0] );
            if( MOSTRO == null ) {
                alert( "Error interno, no se ha encontrado el ID="+api.dialog.params[0] );
                return;
            }

            Formulario.rellenarByMonstro(MOSTRO);    
    
        }
        
    }