class Formulario{
    static id_actual_mostro = 0;
    static input_name =  document.querySelector("#name"); 
    static input_raza =  document.querySelector("#Raza"); 
    static input_pais =  document.querySelector("#land"); 
    static input_debilidad =  document.querySelector("#debilidad"); 
    static input_imgurl =  document.querySelector("#imgurl"); 
    static input_descripcion =  document.querySelector("#descripcion"); 



   static async  openImage ( ) 
    {
        document.querySelector('#inputfile').click();

    }

    static async  loadImage(url){

                const  generateRandomString = (num) => {
                    const characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
                    let result1= '';
                    const charactersLength = characters.length;
                    for ( let i = 0; i < num; i++ ) {
                        result1 += characters.charAt(Math.floor(Math.random() * charactersLength));
                    }
                
                    return result1;
                }

                var fs = require('fs');
                let base64String = url; 
                let base64Image = base64String.split(';base64,').pop();
                let name = "bacalao_"+generateRandomString(9)+".png";


                await fs.writeFileSync(`media/${name}`, base64Image, {encoding: 'base64'});


                Formulario.input_imgurl.src = "../../media/"+name;
                Formulario.input_imgurl.dataset.imagename = name;


    }

    static setNombre(nombre){
       Formulario.input_name.value = nombre;
    }
    static setRaza(raza){
        Formulario.input_raza.value = raza;
    }
    static setPais(pais){
        Formulario.input_pais.value = pais;
    }
    static setDebilidad(debilidad){
        Formulario.input_debilidad.value = debilidad;
    }
    static setImageURL(imgurl){
        Formulario.input_imgurl.src = "../../media/"+imgurl;
        Formulario.input_imgurl.dataset.imagename = imgurl;
    }
    static setDescripcion(descripcion){
        Formulario.input_descripcion.value = descripcion;
    }

    static getNombre(){
        return Formulario.input_name.value;
    }
    static getRaza(){
        return Formulario.input_raza.value;
    }
    static getPais(){
        return Formulario.input_pais.value;
    }
    static getDebilidad(){
        return Formulario.input_debilidad.value;
    }
    static getImgURL(){
        return Formulario.input_imgurl.dataset.imagename;
    }
    static getDescripcion(){
        return Formulario.input_descripcion.value;
    }

    static rellenarByMonstro(mostro){
        Formulario.rellenar(
            mostro.getID(),
            mostro.getNombre(),
            mostro.getRaza(),
            mostro.getPais(),
            mostro.getDebilidad(),
            mostro.getImageURL(),
            mostro.getDescripcion()
        )
    }

    static rellenar(id,nombre,raza,pais,debilidad,imgurl,descripcion){
        Formulario.id_actual_mostro = id;
        Formulario.setNombre(nombre);
        Formulario.setRaza(raza);
        Formulario.setPais(pais);
        Formulario.setDebilidad(debilidad);
        Formulario.setImageURL(imgurl);
        Formulario.setDescripcion(descripcion);
    }

    static async createMonstro( ) 
    {
            const FORMDATA = Formulario.getForm( );

            return Monstro.createMonstro(                     
                FORMDATA.imgurl, 
                FORMDATA.nombre , 
                FORMDATA.raza , 
                FORMDATA.pais ,
                FORMDATA.debilidad ,
                FORMDATA.descripcion
            );
    }

    static getForm(){
        return{
            nombre:Formulario.getNombre(),
            raza:Formulario.getRaza(),
            pais:Formulario.getPais(),
            debilidad:Formulario.getDebilidad(),
            imgurl:Formulario.getImgURL(),
            descripcion:Formulario.getDescripcion()
        };
    }
}