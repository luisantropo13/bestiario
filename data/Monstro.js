class Monstro{
    #id;
    #nombre;
    #raza;
    #pais;
    #debilidad;
    #imgURL;
    #descripcion;
    #fk_stats;
   
    constructor( id , imgURL , nombre , fk_stats , debilidad , descripcion) 
    {
        this.#initMostro( id , imgURL , nombre , fk_stats , debilidad , descripcion );
    }

    async #initMostro( id , imgURL , nombre , fk_stats , debilidad  , descripcion )
    {
        this.#id = id;
        this.#fk_stats = fk_stats;
        this.#nombre = nombre;
        this.#imgURL = imgURL;
        this.#debilidad = debilidad;
        this.#descripcion = descripcion;
        this.#fk_stats = fk_stats;

        if( fk_stats > 0 ) 
        {
            const STATS = await MonstroStatLoader.loadMonstroStatsByID( fk_stats );
            this.#raza = STATS.raza;
            this.#pais = STATS.pais;
        }

    }

    getFKStats( ) 
    { return this.#fk_stats;  }

    getID( ) 
    { return this.#id; }

    getNombre( ) 
    { return this.#nombre; }

    getRaza( ) 
    { return this.#raza; }

    getPais( ) 
    { return this.#pais; }

    getDebilidad( ) 
    { return this.#debilidad; }

    getDescripcion( ) 
    { return this.#descripcion; }

    getImageURL( ) 
    { return this.#imgURL; }


    setNombre( nombre ) 
    { this.#nombre = nombre;}

    setRaza( raza ) 
    { this.#raza = raza; }

    setPais( pais ) 
    { this.#pais = pais; }

    setDebilidad( debilidad ) 
    { this.#debilidad = debilidad; }

    setDescripcion( descripcion ) 
    { this.#descripcion = descripcion; }

    setImageURL( imgURL ) 
    { this.#imgURL = imgURL; }

    async remove( ) 
    {
        await MonstroLoader.deleteMonstroByID( this.getID() );
    }

    async saved( ) 
    {
       await MonstroLoader.saveMonstro(  this );
    }

    toJSON( ) 
    {
        return {
            id: this.getID( ) , 
            nombre: this.getNombre( ) , 

            debilidad: this.getDebilidad( ) , 
            imgURL: this.getImageURL( ) , 
            descripcion: this.getDescripcion( )  ,
            fk_stat: this.#fk_stats   
        };
    }



    modifyByJSON(dataJSON){
        this.setNombre(dataJSON.nombre);
        this.setRaza(dataJSON.raza);
        this.setPais(dataJSON.pais);
        this.setDebilidad(dataJSON.debilidad);
        this.setImageURL(dataJSON.imgurl);
        this.setDescripcion(dataJSON.descripcion);
    }

    static async createMonstro( imgURL , nombre , raza,  pais , debilidad , descripcion ) 
    {
        const STAT = await MonstroStatLoader.createStatMostroAndWrite( raza , pais  );

        return new Monstro( await MonstroLoader.getIDIncrement( ) ,
            imgURL , nombre , STAT , debilidad , descripcion
        );
    }

    static async createFromJSON( data ) 
    {
        const MOSTRO = new Monstro( 
            data.id , 
            data.imgURL , 
            data.nombre , 
            data.fk_stat,
            data.debilidad , 
            data.descripcion
        );

        return MOSTRO;
    }
}